using System.Collections.Generic;
using Xunit;
using ccry_converter_console;

namespace ccry_converter_test
{
    public class ArgumentParserTests
    {
        [Theory]
        [MemberData(nameof(CorrectData))]
        public void Parser_ParseArguments_ReturnParsedArgument(string[] args, string main,
            string money, decimal amount)
        {
            var argumentParser = new ArgumentParser();
            var result = argumentParser.ParseArguments(args);
            Assert.True(result.MainCurrency == main);
            Assert.True(result.MoneyCurrency == money);
            Assert.True(result.Amount == amount);
        }

        [Theory]
        [MemberData(nameof(IncorrectData))]
        public void Parser_ParseArguments_ThrowBusinessException(string[] args, string main,
            string money, decimal amount)
        {
            var argumentParser = new ArgumentParser();
            Assert.Throws<BusinessException>(() => { argumentParser.ParseArguments(args); });
        }

        public static IEnumerable<object[]> CorrectData =>
            new List<object[]>
            {
                new object[] {new[] {"EUR/LTL", "3.4528"}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"eur/ltl", "3.4528"}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"eura/ltla", "3.4528"}, "EUR", "LTL", 3.4528m},
            };

        public static IEnumerable<object[]> IncorrectData =>
            new List<object[]>
            {
                new object[] {new string[] {}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"EUR/LTL"}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"eu/ltl", "3.4528"}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"eu/ltl", "3.4528"}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"eurltl", "3.4528"}, "EUR", "LTL", 3.4528m},
                new object[] {new[] {"eur/ltl", "eur"}, "EUR", "LTL", 3.4528m},
            };
    }
}