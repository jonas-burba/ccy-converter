﻿using System.Collections.Generic;
using Xunit;
using ccry_converter_console;

namespace ccry_converter_test
{
    public class CurrencyConverterTests
    {
        [Theory]
        [MemberData(nameof(Data))]
        public void Converter_ExchangeCurrencyPairs_ReturnCorrectAmounts(string[] args, decimal amount)
        {
            var argumentParser = new ArgumentParser();
            var parsedInput = argumentParser.ParseArguments(args);
            // using same ExchangeRateContext as in console project, because it is fake/mocked/stubbed
            var currencyConverter = new CurrencyConverter(new ExchangeRateReader(new ExchangeRatesContext()));
            var parsedAmount = currencyConverter.Convert(parsedInput);
            Assert.Equal(parsedAmount, amount, 9);
        }

        public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
                new object[] {new[] {"DKK/DKK", "1000"}, 1000.000000000m},
                new object[] {new[] {"EUR/DKK", "1000"}, 7439.400000000m},
                new object[] {new[] {"USD/DKK", "1000"}, 6631.100000000m},
                new object[] {new[] {"DKK/EUR", "1000"}, 134.419442428m},
                new object[] {new[] {"EUR/EUR", "1000"}, 1000.000000000m},
                new object[] {new[] {"USD/EUR", "1000"}, 891.348764685m},
                new object[] {new[] {"DKK/USD", "1000"}, 150.804542233m},
                new object[] {new[] {"EUR/USD", "1000"}, 1121.895311487},
                new object[] {new[] {"USD/USD", "1000"}, 1000.000000000},
            };
    }
}