﻿﻿using System;
using System.Linq;

namespace ccry_converter_console
{
    public class CurrencyConverter
    {
        private readonly IExchangeRateReader _exchangeRateReader;

        public CurrencyConverter(IExchangeRateReader exchangeRateReader)
        {
            _exchangeRateReader = exchangeRateReader;
        }

        public decimal Convert(ExchangeRateIso exchangeInput)
        {
            // x/y = x/DKK / y/DKK
            // assuming that the table is only DKK based with amount denomination stored with data in persistence layer

            const string currency = "DKK";
            var denomination = _exchangeRateReader.GetDenominationOfRatesInDkk();
            var exchangeRates = _exchangeRateReader.GetRatesDenotedInDkk();

            try
            {
                var numerator = exchangeInput.MainCurrency == currency
                    ? 1
                    : exchangeRates.Single(x => x.MainCurrency == exchangeInput.MainCurrency).Amount/denomination;
                var denominator = exchangeInput.MoneyCurrency == currency
                    ? 1
                    : exchangeRates.Single(x => x.MainCurrency == exchangeInput.MoneyCurrency).Amount/denomination;
                return numerator / denominator * exchangeInput.Amount;
            }
            catch
            {
                throw new BusinessException("Currency pair not found");
            }

        }
    }
}
