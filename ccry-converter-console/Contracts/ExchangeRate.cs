﻿﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ccry_converter_console
{
    public class ExchangeRateIso: ExchangeRate
    {
        public string MoneyCurrency { get; set; }
    }

    public class ExchangeRate
    {
        public string MainCurrency { get; set; }
        public decimal Amount { get; set; }

    }

}