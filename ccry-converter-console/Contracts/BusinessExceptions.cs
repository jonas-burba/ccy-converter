﻿using System;

namespace ccry_converter_console
{
    public class BusinessException : Exception
    {
        public BusinessException(string message) : base(message)
        {
        }

        public BusinessException(Exception e) : base(e.Message, e)
        {
        }
    }
}