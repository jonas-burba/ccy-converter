﻿namespace ccry_converter_console
{
    public interface IArgumentParser
    {
        ExchangeRateIso ParseArguments(string[] args);
    }
}