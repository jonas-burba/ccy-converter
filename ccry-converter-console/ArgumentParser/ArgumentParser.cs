﻿using System;
using System.Linq;

namespace ccry_converter_console
{
    public class ArgumentParser : IArgumentParser
    {
        public ExchangeRateIso ParseArguments(string[] args)
        {
            switch (args.Length)
            {
                case 0:
                    throw new BusinessException(
                        "Usage: Exchange <currency pair> <amount to exchange> (without angle brackets)");
                case 2:
                    return Parse(args);
                default:
                    throw new BusinessException("Wrong number of arguments");
            }
        }

        private ExchangeRateIso Parse(string[] args)
        {
            var currencyPair = new string[2];
            try
            {
                currencyPair = args[0].Split("/").Select(x => x.ToUpper().Substring(0, 3)).ToArray();
                if (currencyPair.Length != 2) throw new Exception();
            }
            catch
            {
                throw new BusinessException("Currency pair could not be parsed.");
            }

            if (decimal.TryParse(args[1], out var amount) == false)
            {
                throw new BusinessException("Amount could not be parsed.");
            }

            return new ExchangeRateIso
            {
                MainCurrency = currencyPair[0],
                MoneyCurrency = currencyPair[1],
                Amount = amount,
            };
        }
    }
}