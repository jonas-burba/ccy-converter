﻿namespace ccry_converter_console
{
    public interface IExchangeRatesContext
    {
        ExchangeRate[] ExchangeRates { get; }
    }
}