﻿﻿namespace ccry_converter_console
{
    public class ExchangeRatesContext : IExchangeRatesContext
    {
        // TODO real persistence, file system, database
        public ExchangeRate[] ExchangeRates { get; } = new[]
        {
            new ExchangeRate {MainCurrency="EUR", Amount=743.94m},
            new ExchangeRate {MainCurrency="USD", Amount=663.11m},
            new ExchangeRate {MainCurrency="GDP", Amount=852.85m},
            new ExchangeRate {MainCurrency="SEK", Amount=76.100m},
            new ExchangeRate {MainCurrency="NOK", Amount=78.40m},
            new ExchangeRate {MainCurrency="CHF", Amount=683.58m},
            new ExchangeRate {MainCurrency="JPY", Amount=5.974m},
        };
    }
}