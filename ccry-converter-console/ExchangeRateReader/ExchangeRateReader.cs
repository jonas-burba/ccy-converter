﻿using System;
using System.Linq;

namespace ccry_converter_console
{
    public class ExchangeRateReader : IExchangeRateReader
    {
        private readonly IExchangeRatesContext _exchangeRatesContext;

        public ExchangeRateReader(IExchangeRatesContext exchangeRatesContext)
        {
            _exchangeRatesContext = exchangeRatesContext;
        }

        public ExchangeRate[] GetRatesDenotedInDkk()
        {
            return _exchangeRatesContext.ExchangeRates;
        }

        public decimal GetDenominationOfRatesInDkk()
        {
            return 100m;
        }
    }
}
