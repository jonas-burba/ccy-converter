﻿using System;

namespace ccry_converter_console
{
    public interface IExchangeRateReader
    {
        ExchangeRate[] GetRatesDenotedInDkk();
        decimal GetDenominationOfRatesInDkk();
    }
}