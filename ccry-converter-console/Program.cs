﻿using System;

namespace ccry_converter_console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var argumentParser = new ArgumentParser();
                var parsedArguments = argumentParser.ParseArguments(args);
                var currencyConverter = new CurrencyConverter(new ExchangeRateReader(new ExchangeRatesContext()));
                var convertedAmount = currencyConverter.Convert(parsedArguments);
                Console.WriteLine(convertedAmount);
            }
            catch (BusinessException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey(true);
        }
    }
}
